//print given number is even or odd 
package superX_ass1;
import java.io.*;

public class Day1_code3 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the number : ");
		int i=Integer.parseInt(br.readLine());
		
		if(i%2!=0) {
			System.out.println("It is an odd number");
		}else {
			System.out.println("It is an even number");
		}
	}

}
