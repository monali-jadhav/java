//print all odd number between start=1; end=10

package superX_ass1;

public class Day1_code4 {
	public static void main(String[] args) {
		int start=1;
		int end=10;
		
		for(int i=start; i<=end; i++) {
			if(i%2!=0) {
				System.out.println(i);
			}
		}
	}

}
