//print the given number is prime or composite number

package superX;
import java.io.*;

public class Day2_code3 {
	public static void main(String[] args)throws IOException  {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the number : ");
		int n=Integer.parseInt(br.readLine());
		int count=0;
	
		for(int i=1; i<=n; i++) {
			
			if(n%i==0) {
				count++;
			}
		}
		if(count==2) {
			System.out.println("It is a prime number");
		}else {
			System.out.println("It is a composite number");
		}

	}

}
