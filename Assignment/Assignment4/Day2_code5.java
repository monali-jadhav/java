package superX;
import java.io.*;

public class Day2_code5 {
	    public static void main(String[] args)throws IOException{
	    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	        System.out.println("Enter String: ");
	        String str = br.readLine();
	        char[] ch = str.toCharArray();
	        int a = 0;
	        int e = 0;
	        int i = 0;
	        int o = 0;
	        int u = 0;

	        for(int x = 0;x<ch.length;x++){
	            if(ch[x] == 'a'){
	                a++;
	            }else if(ch[x] == 'e'){
	                e++;
	            }else if(ch[x] == 'i'){
	                i++;
	            }else if(ch[x] == 'o'){
	                o++;
	            }else if(ch[x] == 'u'){
	                u++;
	            }
	        }
	        System.out.println("a = "+a);
	        System.out.println("e = "+e);
	        System.out.println("i = "+i);
	        System.out.println("o = "+o);
	        System.out.println("u = "+u);
	        
	    }
}
