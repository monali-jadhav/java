/*
 1 
2 4 
3 6 9 
4 8 12 16 



*/
package superX;
import java.io.*;

public class Day2_code2 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the row input: ");
		int n=Integer.parseInt(br.readLine());
		
		int n1=1;
		for(int i=1; i<=n; i++) {
			for(int j=1; j<=i; j++) {
				System.out.print(n1*j+" ");
			}
			n1++;
			System.out.println();
		}
	}

}
