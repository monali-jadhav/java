/*
	A B C D 
	B C D E 
	C D E F 
	D E F G 
*/
package superX;
import java.io.*;

public class Day2_code1 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the input : ");
		int n =Integer.parseInt(br.readLine());
		char ch = 'A';
		char ch1 = 'A';
		
		for(int i=0; i<n; i++) {
			ch = ch1;
			for(int j=0; j<n; j++) {
				System.out.print(ch+" ");
				ch++;
			}
			ch1++;
			System.out.println();
		}
	}

}
