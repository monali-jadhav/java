//print the number is a strong number or not
package superX;
import java.io.*;

public class Day5_code3 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the number: ");
		int n= Integer.parseInt(br.readLine());
		
		int temp=n;
		int sum=0;
		while(n != 0){
            int rem = n%10;
            int mul = 1;
            for(int i = 1;i<=rem;i++){
                mul = mul*i;
            }
            sum = sum + mul;
             n = n/10;
        }
        if(temp == sum){
            System.out.println(temp + "  is strong number");
        }else{
            System.out.println(temp + "  is not strong number");
        }

	}

}
