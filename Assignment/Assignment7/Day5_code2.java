/*
a 
A B 
a b c 
A B C D 
*/
package superX;
 
import java.io.*;

public class Day5_code2 {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the row: ");
		int n = Integer.parseInt(br.readLine());
		
		for(int i=1; i<=n; i++) {
			char ch1='a';
			char ch2='A';
			for(int j=1; j<=i; j++) {
				if(i%2!=0) {
					System.out.print(ch1+" ");
					ch1++;
				}else {
					System.out.print(ch2+" ");
					ch2++;
				}
			}
			System.out.println();
		}
	}

}
