/*
1 3 5 7 
2 4 6 8 
9 11 13 15 
10 12 14 16 
 */
package superX;
import java.io.*;

public class Day5_code4 {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the row: ");
		int n = Integer.parseInt(br.readLine());
		
		int n1=1;
		int n2=2;
		for(int i=1;i<=n;i++) {
			for(int j=1; j<=n; j++) {
				if(i%2!=0) {
					System.out.print(n1+" ");
					n1=n1+2;
				}else {
					System.out.print(n2+" ");
					n2=n2+2;
				}
			}
			System.out.println();
		}
	}

}
