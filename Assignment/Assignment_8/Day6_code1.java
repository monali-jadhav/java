/*
 A B C D
 1 3 5 7
 A B C D
 9 11 13 15
 A B C D
 */
package superX;
import java.io.*;

public class Day6_code1 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the number");
		int n= Integer.parseInt(br.readLine());
		
		int n1=1;
		
		for(int i=1; i<=n; i++) {
			char ch='A';
			for(int j=1; j<n; j++) {
				if(i%2!=0) {
					System.out.print(ch+" ");
					ch++;
				}else {
					System.out.print(n1+" ");
					n1=n1+2;
				}
			}
			System.out.println();
		}
		
	}

}
