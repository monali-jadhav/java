//WAP to check whether given number is perfect or not
package superX;
import java.io.*;

public class Day6_code3 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the number : ");
		int n= Integer.parseInt(br.readLine());
		
		int sum=0;
		for(int i = 1;i<n;i++){
            if(n%i == 0){
                sum = sum + i;
            }
        }
        if(n == sum){
            System.out.println(n+" is perfect number");
        }else{
            System.out.println(n+" is not perfect number");
        }

	}

}
