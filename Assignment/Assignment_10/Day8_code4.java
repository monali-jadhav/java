package superX;

import java.util.*;

public class Day8_code4 {
	void fibo(int n){
        int n1 = 0;
        int n2 = 1;
        int n3 = 0;
        for(int i=1;i<=n;i++){
            System.out.println(n3);
            n1 = n2;
            n2 = n3;
            n3 = n1+n2;
        }
    }
    public static void main(String[] args) {
    	Day8_code4 obj = new Day8_code4();
    	Scanner sc = new Scanner(System.in);
    	System.out.println("enter the number: ");
    	int n = sc.nextInt();
    	
        obj.fibo(n);
    }
}
