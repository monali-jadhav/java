package superX;
import java.io.*;

public class Day4_code4 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the start number: ");
		int n1 = Integer.parseInt(br.readLine());
		System.out.println("enter the end number");
		int n2 = Integer.parseInt(br.readLine());
		
		int sum =0;
		for(int i=n1; i<=n2; i++ ) {
			sum=sum+i;
		}
		System.out.println(sum);
	}

}
