//print the factorial of given number
package superX;
import java.io.*;

public class Day4_code3 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the number: ");
		int n= Integer.parseInt(br.readLine());
		
		int fact=1;
		for(int i=1; i<=n; i++) {
			fact=fact*i;
			
		}
		System.out.println("factorial of given number is "+fact);
	}

}
