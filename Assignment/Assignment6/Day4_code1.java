/*
A B C D 
# # # # 
A B C D 
# # # # 
A B C D 
*/
package superX;
import java.io.*;

public class Day4_code1 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the number: ");
		int n= Integer.parseInt(br.readLine());
		
		
		for(int i=1; i<=n; i++) {
			char ch='A';
			for(int j=1; j<n; j++) {
				if(i%2!=0) {
					System.out.print(ch+" ");
					ch++;
				}else {
					System.out.print("#"+" ");
				}
			}
			System.out.println();
		}
		
	}

}
