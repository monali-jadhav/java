package superX;
import java.io.*;

public class Day4_code5 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the String: ");
		String str =br.readLine();
		
		char ch[]=str.toCharArray();
		for(int i=0; i<ch.length; i++) {
			if(ch[i]>=65 && ch[i]<=95) 
				ch[i]=(char)(ch[i]+32);
			else 
				ch[i]=(char)(ch[i]-32);
			
		}
		System.out.println(ch);
		
	}

}
