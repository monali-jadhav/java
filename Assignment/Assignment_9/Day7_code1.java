/*
1 2 3 4 
a b c d 
5 6 7 8 
e f g h 
9 10 11 12 */
package superX;
import java.io.*;

public class Day7_code1 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the number");
		int n=Integer.parseInt(br.readLine());
		
		int n1=1;
		char ch='a';
		for(int i=1; i<=n; i++) {
			for(int j=1; j<n; j++) {
				if(i%2 != 0) {
					System.out.print(n1+" ");
					n1++;
				}else {
					System.out.print(ch+" ");
					ch++;
				}
			}
			System.out.println();
		}
	}

}
