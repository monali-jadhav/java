/*
A B C D 
D C B A 
A B C D 
D C B A  */

package superX;
import java.io.*;

public class Day3_code1 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the row: ");
		int n = Integer.parseInt(br.readLine());
		
		for(int i=1;i<=n; i++) {
			char ch='A';
			char ch1='D';
			for(int j=1; j<=n; j++) {
				if(i%2==0) {
					System.out.print(ch1+" ");
					ch1--;
				}else {
					System.out.print(ch+" ");
					ch++;
				}
			}
			System.out.println();
		}
	}
	

}
