//write a program to check whether the string contains characters other than letters
package superX;
import java.io.*;

public class Day3_code5 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("enter the string  : ");
		String str=br.readLine();
		
		char ch[]=str.toCharArray();
		System.out.println("string contains the letters:");
		for(int i=0; i<ch.length; i++) {
			 if((ch[i] >= 65 && ch[i] <= 90)||(ch[i] >= 97 && ch[i] <= 122)){
				 continue;
			 }else {
				 System.out.println(ch[i]+" ");
			 }
			 
		}
		System.out.println();
	}

}
