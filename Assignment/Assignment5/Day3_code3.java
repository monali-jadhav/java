///write code to check given number is palindrome or not
package superX;
import java.io.*;

public class Day3_code3 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the number");
		int n= Integer.parseInt(br.readLine());
		int count=0;
		int sum=0;
		int temp=n;
		
		for(int i=0; i<=n; i++) {
			int rem=n%10;
			sum=(sum*10)+rem;
			n=n/10;
		}
		
		if(temp==sum) {
			System.out.println("number is palindrome");
		}else {
			System.out.println("number is not palindrome");
		}
		
	}

}
