//write to reverse the given number
package superX;

public class Day3_code4 {
	public static void main(String[] args) {
		int start=25435;
		int end=25449;
		
		
		for(int i=start; i<=end;i++) {
			int rev=0;
			int n= i;
			while(n!=0) {
				int rem=n%10;
				rev=rev*10+rem;
				n=n/10;
			}
				
			System.out.println("The reverse number of given number is: "+rev);
		}
		
	}

}
