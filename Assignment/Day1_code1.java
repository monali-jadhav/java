/*
 1 2 3 4
 2 3 4 5
 3 4 5 6
 4 5 6 7
 */

package superX_ass1;
import java.io.*;

public class Day1_code1 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the number");
		int n =Integer.parseInt(br.readLine());
		int a=1;
		
		for(int i=0; i<=n; i++) {
		
			for(int j=0; j<=n; j++) {
				System.out.print(a+" ");
				a++;
			}
			a=n+i-1;
			System.out.println();
		}
		
	}

}
