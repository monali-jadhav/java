import java.util.*;
public class smallest {
    static int[] number(int arr[], int n){
        int min=Integer.MAX_VALUE;
        int min_2=Integer.MAX_VALUE;
        for(int i=0; i<arr.length; i++){
            if(arr[i] < min){
                min=arr[i];
            }
        }
        // System.out.println(min);
        for(int i=0; i<arr.length; i++){
            if(arr[i] < min_2 && arr[i] > min){
                min_2=arr[i];
            }
        }
        //System.out.println(min_2);
        int[] ret = {min, min_2};
        return ret;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter array size");
        int size = sc.nextInt();
        int arr[]=new int[size];
        System.out.println("Enter array element");
        for(int i=0; i<arr.length; i++){
            arr[i]=sc.nextInt();
        }
    
        int[] ret=number(arr, arr.length);
        for(int i=0; i<ret.length; i++){
            System.out.print(ret[i]+" ");
        }

    }
}
