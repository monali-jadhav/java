import java.util.*;
class FirstRepeat {
    int firstrepeatele(int arr[], int n){
        for(int i=0; i<arr.length; i++){
            for(int j=i+1; j<arr.length; j++){
                if(arr[i]==arr[j]){
                    return i+1;
                }
            }
            
        }
        return -1;
        
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        System.out.println("Enter array size");
        int size = sc.nextInt();
        int arr[]= new int[size];
        System.out.println("Enter array element");
        for(int i=0; i<arr.length; i++){
            arr[i]=sc.nextInt();
        }

        FirstRepeat fr = new FirstRepeat();
        System.out.println("first repeating element is "+fr.firstrepeatele(arr, arr.length));
        
    }
    
}
