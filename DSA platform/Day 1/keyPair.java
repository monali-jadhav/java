import java.util.*;
class keyPair {
    boolean keypair(int arr[], int n, int x){
        
        Arrays.sort(arr);

        int start = 0;
        int end = n - 1;

        while (start < end) {
            int currentSum = arr[start] + arr[end];

            if (currentSum == x) {
                return true;
            } else if (currentSum < x) {
                start++;
            } else {
                end--;
            }
        }

        return false;
    }
   public static void main(String[] args){
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter value of x");
    int x = sc.nextInt();
    System.out.println("Enter array size");
    int size = sc.nextInt();
    int arr[]=new int[size];
    System.out.println("Enter array element");
    for(int i=0; i<arr.length; i++){
        arr[i]=sc.nextInt();
    }
    
    keyPair k = new keyPair();
    boolean ret = k.keypair(arr, arr.length, x);
    if(ret == true){
        System.out.println("YES");
    }else{
        System.out.println("NO");
    }

   } 
}
